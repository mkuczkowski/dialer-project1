# Dialer-Project1
Web app that allows users to make phone calls through browser.
### Getting started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

#### Prerequisites
* [Node.js (v8.x)](https://nodejs.org/en/) - JavaScript runtime environment
* [npm (v5.x)](https://www.npmjs.com/) - Package manager for Node.js
#### Installation
Open terminal and enter your downloaded project folder using cd command. 
For example:

```sh
cd C:\Users\username\desktop\Dialer-project1
```
Before starting an app make sure to set correct login and password in app.js file:
```sh
const dialerLogin = "ENTER YOUR DIALER LOGIN HERE";
const dialerPassword = "ENTER YOUR DIALER PASSWORD HERE";
```
Make sure that your project path is correct and then run following commands in terminal:
```sh
npm install
node app.js
```
If everything is running correctly, the Command Interpreter displays:
```sh
app listening on port 3000
```

#### Making calls
We can try our app now by making phone calls through browser. For example we can use [restninja](https://restninja.io/) to make a POST request.
To do that we'll have to switch request type from GET to POST and set request uri on this website to:
```sh
http://localhost:3000/call/
```
On the top-right corner set "ajax" and in body section set body type to json with phone numbers info:
```sh
{
 "number1": "123456789",
 "number2": "999999999"
}
```
Make sure to put correct phone numbers - if you want to call yourself then set the other number to 999999999.
Switch from "body" section to "headers" and set header to:
```sh
content-type
```
and value:
```sh
application/json
```
Now we can click "Send". If everything has been done correctly then we should get a response with an id and success - for example:
```sh
{
  "id": 0,
  "success": true
}
```
Each call will have its own id which allows us to see current connection status of any call that has been made.
To do that we have to go to this address in our browser:

```sh
http://localhost:3000/status/0
```
In the example above "0" is an id of a call that's been made. If we'll put a value that's not a correct id number then we'll get a response with an error.  

#### Process manager  
If we want to monit our app via process manager, we'll have to install PM2.  
To install PM2 globally use following command:    
```sh
npm install pm2 -g
```  
Make sure that your're in the project folder in terminal and start process manager by running:  
```sh
pm2 start manager.config.js
```  
Now we can check out our app status:  
```sh
pm2 status
```  
To see process list and global logs, use command:  
```sh
pm2 monit
```  
