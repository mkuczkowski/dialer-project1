const express = require('express');
const app = express();
const Dialer = require('dialer').Dialer;
const cors = require('cors');
const bodyParser = require('body-parser');
const http = require('http').Server(app);
const io = require('socket.io')(http);

const dialerLogin = "ENTER YOUR DIALER LOGIN HERE";
const dialerPassword = "ENTER YOUR DIALER PASSWORD HERE";

const config = {
 url: 'https://uni-call.fcc-online.pl',
 login: dialerLogin,
 password: dialerPassword
};

app.use(cors());
app.use(bodyParser.json());

Dialer.configure(config);

http.listen(3000, () => {
 console.log('app listening on port 3000');
});

let numberOfUsers = 0;

io.on('connection', (socket) => {
    console.log('user connected');
    io.emit('users', ++numberOfUsers);
    socket.on('disconnect', () => {
        io.emit('users', --numberOfUsers);
        console.log('user disconnected');
        socket.broadcast.emit('message', 'User disconnected!');
    });
    socket.on('message', (message) => {
        io.emit('message', message);
        console.log('message', message)
    });
    socket.on('status', (status) => {
        console.log('status', status)
    });
    socket.broadcast.emit('message', 'User connected!');
       
});

let bridge;
let currentStatus;
let identifierToAssign = 0;
const connections = [];

app.post('/call/', async (req, res) => {
    const currentCallId = identifierToAssign;
    const body = req.body; 
    try {
        bridge = await Dialer.call(body.number1, body.number2);
    } catch (error) {
        res.json({succes: false, error: error});
        return;
    }
    identifierToAssign++;
    connections.push({
        id: currentCallId,
        status: currentStatus
    });
    let interval = setInterval(async () => {
        let status;
        try {
            status = await bridge.getStatus();
        } catch (error) {
            status = "FAILED";
        }
        if(currentStatus !== status) {
            currentStatus = status;
            io.emit('status', status);
            const currentCallIndex = connections.findIndex( call => call.id === currentCallId );
            connections[currentCallIndex] = {
                id: currentCallId,
                status: currentStatus
            };
            console.log(connections[currentCallIndex]);
        }
        console.log('Call started...');
        if(currentStatus === "ANSWERED" || currentStatus === "FAILED"
        || currentStatus === "BUSY" || currentStatus === "NO ANSWER") {
            console.log('Call ended - interval cleared');
            clearInterval(interval);
        }
    }, 500);
    
    res.json({id: currentCallId, success: true});
});

app.get('/status/:callsId', (req, res) => {
    const requestedId = req.params.callsId;
    const requestedStatusIndex = connections.findIndex( call => call.id == requestedId );
    if(connections[requestedStatusIndex] !== undefined && connections[requestedStatusIndex] !== null) {
        res.json(connections[requestedStatusIndex]);
    } else {
        res.json({error: "requested id does not exist"});
    }
});